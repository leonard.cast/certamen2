import { Request, Response } from 'express'
import * as moviesRepository from '../repository/moviesRepository'

export const sendMoviesInfo = (request: Request, response: Response) => {
    response.json(moviesRepository.getMoviesData())
}